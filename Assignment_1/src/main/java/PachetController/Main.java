package PachetController;

import PachetModel.*;
import PachetView.*;

public class Main {

	public static void main(String[] args) {
		
		Model md = new Model();
		View vw = new View();
		@SuppressWarnings("unused")
		Controller cr = new Controller(md,vw);
		
	}

}
