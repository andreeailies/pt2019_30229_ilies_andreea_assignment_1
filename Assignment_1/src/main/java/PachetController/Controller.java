package PachetController;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import PachetModel.*;
import PachetView.*;

public class Controller {

	private Model model;
	private View view;

	/**
	 * Constructorul partii de controller a aplicatiei.
	 * @param model modelul aplicatiei
	 * @param view view-ul aplicatiei
	 */
	public Controller(Model model, View view) {
		this.model = model;
		this.view = view;

		view.addAdunareListener(new AdunareListener());
		view.addScadereListener(new ScadereListener());
		view.addInmultireListener(new InmultireListener());
		view.addImpartireListener(new ImpartireListener());
		view.addDerivareListener(new DerivareListener());
		view.addIntegrareListener(new IntegrareListener());
	}

	class AdunareListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			String p1 = "";
			String p2 = "";
			Polinom polinom1, polinom2;
			String rezultat = "";
			p1 = view.getP1();
			p2 = view.getP2();
			polinom1 = model.transformare(p1);
			polinom2 = model.transformare(p2);
			rezultat = polinom1.adunare(polinom2).toString();
			view.setCerinta(" P1 + P2 ");
			view.setRez("<html>" + rezultat + "</html>");
		}
	}

	class ScadereListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			String p1 = "";
			String p2 = "";
			Polinom polinom1, polinom2;
			String rezultat = "";
			p1 = view.getP1();
			p2 = view.getP2();
			polinom1 = model.transformare(p1);
			polinom2 = model.transformare(p2);
			rezultat = polinom1.scadere(polinom2).toString();
			view.setCerinta(" P1 - P2 ");
			view.setRez("<html>" + rezultat + "</html>");
		}
	}

	class InmultireListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			String p1 = "";
			String p2 = "";
			Polinom polinom1, polinom2;
			String rezultat = "";
			p1 = view.getP1();
			p2 = view.getP2();
			polinom1 = model.transformare(p1);
			polinom2 = model.transformare(p2);
			rezultat = polinom1.inmultire(polinom2).toString();
			view.setCerinta(" P1 * P2 ");
			view.setRez("<html>" + rezultat + "</html>");
		}
	}

	class ImpartireListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			String p1 = "";
			String p2 = "";
			Polinom polinom1, polinom2;
			String cat = "";
			String rest = "";
			p1 = view.getP1();
			p2 = view.getP2();
			polinom1 = model.transformare(p1);
			polinom2 = model.transformare(p2);
			cat = polinom1.impartire(polinom2).get(0).toString();
			rest = polinom1.impartire(polinom2).get(1).toString();
			view.setCerinta(" P1 / P2 ");
			view.setRez("<html> Catul: " + cat + "<br> Restul: " + rest + "</html>");
		}
	}

	class DerivareListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			String p1 = "";
			Polinom polinom1;
			String rezultat = "";
			p1 = view.getP1();
			polinom1 = model.transformare(p1);
			rezultat = polinom1.derivare().toString();
			view.setCerinta(" P1 derivat ");
			view.setRez("<html>" + rezultat + "</html>");
		}
	}

	class IntegrareListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			String p1 = "";
			Polinom polinom1;
			String rezultat = "";
			p1 = view.getP1();
			polinom1 = model.transformare(p1);
			rezultat = polinom1.integrare().toString();
			view.setCerinta(" P1 integrat ");
			view.setRez("<html>" + rezultat + "</html>");
		}
	}
}
