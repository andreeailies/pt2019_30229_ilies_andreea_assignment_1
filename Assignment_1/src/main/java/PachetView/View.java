package PachetView;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class View extends JFrame {

	private static final long serialVersionUID = 1L;

	private JPanel panouBaza = new JPanel();
	private JPanel panouButoane = new JPanel();
	private JPanel panouInput = new JPanel();
	private JPanel panouOutput = new JPanel();

	private JLabel titlu = new JLabel("Procesarea Polinoamelor");
	private JLabel unu = new JLabel("P1:");
	private JLabel doi = new JLabel("P2:");
	private JLabel cerinta = new JLabel(" P1 + P2 ");
	private JLabel rez = new JLabel("0",SwingConstants.CENTER);

	private JTextField p1 = new JTextField(20);
	private JTextField p2 = new JTextField(20);

	private JButton plus = new JButton(" + ");
	private JButton minus = new JButton(" - ");
	private JButton inm = new JButton(" * ");
	private JButton imp = new JButton(" / ");
	private JButton deriv = new JButton("dx");
	private JButton integr = new JButton("int");

	public View() {
		this.setContentPane(panouBaza);
		this.pack();
		this.setTitle("Procesare polinoame");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
		this.setMinimumSize(new Dimension(350, 400));

		panouBaza.setLayout(new BoxLayout(panouBaza, BoxLayout.Y_AXIS));
		panouBaza.setBackground(new Color(66, 134, 244));
		
		panouBaza.add(new JLabel(" "));
		panouBaza.add(titlu);
		panouBaza.add(new JLabel(" "));
		panouBaza.add(panouInput);
		panouBaza.add(new JLabel(" "));
		panouBaza.add(panouButoane);
		panouBaza.add(panouOutput);

		antet();
		zonaInput();
		zonaButoane();
		zonaOutput();
	}

	/**
	 * Antetul ferestrei
	 */
	private void antet() {
		titlu.setFont(new Font("Courier New", Font.BOLD, 20));
		titlu.setForeground(Color.white);
		titlu.setAlignmentX(CENTER_ALIGNMENT);
	}

	/**
	 * Zona de input a ferestrei. 
	 */
	private void zonaInput() {
		panouInput.setBackground(new Color(66, 134, 244));
		panouInput.setLayout(new BoxLayout(panouInput,BoxLayout.Y_AXIS));
		panouInput.setMaximumSize(new Dimension(350, 80));
		panouInput.setAlignmentX(CENTER_ALIGNMENT);
		
		JPanel j1 = new JPanel();
		j1.setMinimumSize(new Dimension(350, 35));
		j1.setBackground(new Color(66, 134, 244));
		j1.setLayout(new FlowLayout());
		JPanel j2 = new JPanel();
		j2.setMinimumSize(new Dimension(350, 35));
		j2.setBackground(new Color(66, 134, 244));
		panouInput.add(j1);
		panouInput.add(j2);
		j1.add(unu);
		j1.add(p1);
		j2.add(doi);
		j2.add(p2);
		
		unu.setForeground(Color.orange);
		doi.setForeground(Color.yellow);
	}

	/**
	 * Zona butoanelor din fereastra
	 */
	private void zonaButoane() {
		panouButoane.setBackground(new Color(66, 134, 244));
		panouButoane.setLayout(new GridLayout(2, 3));
		panouButoane.setMaximumSize(new Dimension(150, 120));
		panouButoane.setAlignmentX(CENTER_ALIGNMENT);
		panouButoane.add(plus);
		panouButoane.add(inm);
		panouButoane.add(deriv);
		panouButoane.add(minus);
		panouButoane.add(imp);
		panouButoane.add(integr);
		
		plus.setBackground(Color.black);
		plus.setForeground(Color.white);
		minus.setBackground(new Color(249, 242, 29));
		minus.setForeground(Color.black);
		inm.setBackground(new Color(226, 6, 14));
		inm.setForeground(Color.black);
		imp.setBackground(new Color(0, 193, 83));
		imp.setForeground(Color.white);
		deriv.setBackground(new Color(255, 141, 28));
		deriv.setForeground(Color.white);
		integr.setBackground(new Color(29, 200, 226));
		integr.setForeground(Color.black);
	}

	/**
	 * Zona de afisare a rezultatelor
	 */
	private void zonaOutput() {
		panouOutput.setBackground(new Color(66, 134, 244));
		panouOutput.setLayout(new BoxLayout(panouOutput,BoxLayout.Y_AXIS));
		panouOutput.setMaximumSize(new Dimension(350, 120));
		panouOutput.setAlignmentX(CENTER_ALIGNMENT);
		
		panouOutput.add(new JLabel(" "));
		panouOutput.add(cerinta);
		panouOutput.add(new JLabel(" "));
		panouOutput.add(rez);
		
		cerinta.setBackground(Color.white);
		cerinta.setOpaque(true);
		rez.setForeground(Color.white);
		
		cerinta.setAlignmentX(CENTER_ALIGNMENT);
		rez.setAlignmentX(CENTER_ALIGNMENT);
	}
	
	public String getP1() {
		return p1.getText();
	}
	
	public String getP2() {
		return p2.getText();
	}
	
	public void setRez(String x) {
		rez.setText(x);
	}
	
	public void setCerinta(String x) {
		cerinta.setText(x);
	}
	
	public void addAdunareListener(ActionListener x) {
        plus.addActionListener(x);
    }
	public void addScadereListener(ActionListener x) {
        minus.addActionListener(x);
    }
	public void addInmultireListener(ActionListener x) {
        inm.addActionListener(x);
    }
	public void addImpartireListener(ActionListener x) {
        imp.addActionListener(x);
    }
	public void addDerivareListener(ActionListener x) {
        deriv.addActionListener(x);
    }
	public void addIntegrareListener(ActionListener x) {
        integr.addActionListener(x);
    }

}
