package PachetModel;

public class Monom implements Comparable<Monom>, Cloneable {

	private int putere = 0;
	private float coeficient = 0;

	public Monom(float coeficient, int putere) {
		this.setPutere(putere);
		this.setCoeficient(coeficient);
	}

	/**
	 * Aduna doi monomi
	 * @param doi al doilea termen al sumei
	 * @return returneaza un Monom reprezentand suma
	 */
	public Monom adunare(Monom doi) {
		if (this.getPutere() != doi.getPutere()) {
			return null;
		} else {
			Monom rez = new Monom(this.getCoeficient() + doi.getCoeficient(), this.getPutere());
			return rez;
		}
	}

	/**
	 * Scade doi monomi
	 * @param doi scazatorul
	 * @return returneaza un Monom reprezentand diferenta
	 */
	public Monom scadere(Monom doi) {
		if (this.getPutere() != doi.getPutere()) {
			return null;
		} else {
			Monom rez = new Monom(this.getCoeficient() - doi.getCoeficient(), this.getPutere());
			return rez;
		}
	}

	/**
	 * Inmulteste doi monomi
	 * @param doi al doilea termen al produsului
	 * @return returneaza un Monom reprezentand produsul
	 */
	public Monom inmultire(Monom doi) {
		if (this.getCoeficient() == 0 || doi.getCoeficient() == 0) {
			return null;
		}
		Monom rez = new Monom(this.getCoeficient() * doi.getCoeficient(), this.getPutere() + doi.getPutere());
		return rez;
	}

	/**
	 * Imparte doi monomi
	 * @param doi impartitorul
	 * @return returneaza un Monom reprezentand raportul dintre cei doi
	 */
	public Monom impartire(Monom doi) {
		if (this.getPutere() < doi.getPutere() || doi.getCoeficient() == 0) {
			return null;
		} else {
			Monom rez = new Monom(this.getCoeficient() / doi.getCoeficient(), this.getPutere() - doi.getPutere());
			return rez;
		}
	}

	/**
	 * Deriveaza un Monom
	 * @return returneaza un Monom reprezentand derivata
	 */
	public Monom derivare() {
		if (this.getPutere() == 0) return null;
		Monom rez = new Monom(this.getCoeficient() * this.getPutere(), this.getPutere() - 1);
		return rez;
	}

	/**
	 * Integreaza un Monom
	 * @return returneaza un Monom reprezentand integrala din cel original
	 */
	public Monom integrare() {
		if (this.getCoeficient() == 0) return null;
		Monom rez = new Monom(this.getCoeficient() / (this.getPutere() + 1), this.getPutere() + 1);
		return rez;
	}

	public int getPutere() {
		return putere;
	}

	public void setPutere(int putere) {
		this.putere = putere;
	}

	public float getCoeficient() {
		return coeficient;
	}

	public void setCoeficient(float coeficient) {
		this.coeficient = coeficient;
	}

	@Override
	public int compareTo(Monom x) {
		if (this.getPutere() > x.getPutere()) {
			return -1;
		} else if (this.getPutere() == x.getPutere()) {
			return 0;
		}
		return 1;
	}
	
	public Object clone() {
		Monom x = new Monom(0,0);
		x.setCoeficient(this.coeficient);
		x.setPutere(this.putere);
		return x;
	}

}
