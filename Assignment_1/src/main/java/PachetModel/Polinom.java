package PachetModel;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.TreeSet;

public class Polinom implements Cloneable {

	private TreeSet<Monom> monomi = new TreeSet<Monom>();

	/**
	 * Adauga sau aduna un Monom nou obiectului Polinom
	 * @param adauga obiect Monom de adaugat la Polinom
	 */
	public void addMonom(Monom adauga) {
		if (adauga == null) return;
		if (adauga.getCoeficient() == 0) {
			return;
		} else {
			for (Monom m : this.monomi) {

				if (m.getPutere() == adauga.getPutere()) {
					float auxCoef = m.getCoeficient() + adauga.getCoeficient();

					if (auxCoef == 0.0) {
						this.monomi.remove(m);
						return;
					} else {
						m.setCoeficient(auxCoef);
						return;
					}
				} else {
					if (m.getPutere() < adauga.getPutere()) {
						break;
					}
				}
			}

			this.monomi.add(adauga);
		}
	}

	/**
	 * Aduna doua polinoame
	 * @param doi termenul al doilea al sumei
	 * @return returneaza un obiect Polinom, reprezentand suma celor doua polinoame
	 */

	public Polinom adunare(Polinom doi) {
		Polinom rez = new Polinom();
		for (Monom m : this.monomi) {
			rez.addMonom((Monom) m.clone());
		}
		for (Monom m : doi.getMonomi()) {
			rez.addMonom((Monom) m.clone());
		}
		return rez;
	}

	/**
	 * Scade doua polinoame
	 * @param doi scazatorul
	 * @return returneaza un obiect Polinom, reprezentand diferenta celor doua polinoame
	 */
	public Polinom scadere(Polinom doi) {
		Polinom rez = new Polinom();
		for (Monom m : this.monomi) {
			rez.addMonom((Monom) m.clone());
		}
		for (Monom m : doi.getMonomi()) {
			rez.addMonom(new Monom(-m.getCoeficient(), m.getPutere()));
		}
		return rez;
	}

	/**
	 * Inmulteste doua polinoame
	 * @param doi al doilea termen al produsului
	 * @return returneaza un Polinom echivalent cu produsul
	 */
	public Polinom inmultire(Polinom doi) {
		Polinom rez = new Polinom();
		for (Monom m1 : this.monomi) {
			for (Monom m2 : doi.getMonomi()) {
				rez.addMonom(m1.inmultire(m2));
			}
		}
		return rez;
	}

	/**
	 * Imparte doua polinoame
	 * @param doi impartitorul
	 * @return returneaza o lista continand la indexul 0 catul, iar la indexul 1 restul
	 */
	public ArrayList<Polinom> impartire(Polinom doi) {
		if (doi == null) return null;
		Polinom cat = new Polinom();
		Polinom rest = null;
		try {
			rest = (Polinom) this.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}

		try {
		while (rest != null && rest.getMonomi().first().getPutere() >= doi.getMonomi().first().getPutere()) {
			Polinom aux = new Polinom();
			aux.addMonom(rest.getMonomi().first().impartire(doi.getMonomi().first()));
			cat = cat.adunare(aux);
			rest = rest.scadere(aux.inmultire(doi));
			if (rest.getMonomi().isEmpty()) {
				break;
			}
		}
		} catch(Exception e) {
			
		}

		ArrayList<Polinom> rez = new ArrayList<>();
		rez.add(cat);
		rez.add(rest);
		return rez;
	}

	/**
	 * Deriveaza obiectul Polinom
	 * @return returneaza un Polinom reprezentand derivata celui original
	 */
	public Polinom derivare() {
		Polinom rez = new Polinom();
		for (Monom m : this.monomi) {
			rez.addMonom(m.derivare());
		}
		return rez;
	}

	/**
	 * Integreaza obiectul Polinom
	 * @return returneaza un Polinom reprezentand integrala celui original
	 */
	public Polinom integrare() {
		Polinom rez = new Polinom();
		for (Monom m : this.monomi) {
			rez.addMonom(m.integrare());
		}
		return rez;
	}

	public TreeSet<Monom> getMonomi() {
		return monomi;
	}

	public void setMonomi(TreeSet<Monom> monomi) {
		this.monomi = monomi;
	}

	/**
	 * Transforma obiectul Polinom intr-un String lizibil
	 */
	public String toString() {
		String polinom = "";
		NumberFormat formatter = new DecimalFormat("#0.00");
		if (this.getMonomi().isEmpty()) {
			return "0";
		}
		for (Monom m : this.getMonomi()) {
			if (m.getCoeficient() > 0) {
				polinom += "+";
			}
			polinom += formatter.format(m.getCoeficient()) + " * x^" + m.getPutere() + " ";
		}
		if (this.getMonomi().last().getPutere() == 0) {
			polinom = polinom.substring(0, polinom.length() - 7);
		}

		polinom += "\n";
		return polinom;
	}

	/**
	 * Afiseaza un Polinom in consola.
	 */
	public void afisare() {
		System.out.println(this);
	}
}
