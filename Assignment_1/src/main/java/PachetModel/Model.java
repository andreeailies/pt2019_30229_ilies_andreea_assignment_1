package PachetModel;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Model {

	private Pattern p = Pattern.compile("(-?\\b\\d+)([x])?((\\^)?(\\d+\\b))?");

	/**
	 * Transforma un String in obiectul Polinom echivalent
	 * @param input String de transformat
	 * @return returneaza un obiect Polinom echivalent cu String-ul primit ca parametru
	 */
	public Polinom transformare(String input) {
		int coeficient = 0;
		int putere = 0;
		Polinom poli = new Polinom();
		
		input = input.replaceAll("(\\s)|(\\*)|([a-zA-Z&&[^x]])", "");
		input = input.replaceAll("(\\+x)|(^x)", "\\+1x");
		input = input.replaceAll("\\-x", "\\-1x");
		Matcher m = p.matcher(input);
		
		while(m.find()) {
			String putereS = m.group(5);
			if (m.group(5) == null) {
				if (m.group(2) ==  null ) {
					putereS = "0";
				} else {
					putereS = "1";
				}
			}
			putere = Integer.parseInt(putereS);
			coeficient = Integer.parseInt(m.group(1));
			poli.addMonom(new Monom(coeficient, putere));
		}
		return poli;
	}
	
}
