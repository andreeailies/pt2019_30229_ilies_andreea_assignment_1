package PachetModel;

import static org.junit.Assert.*;

import org.junit.Test;

public class MonomTest {

	@Test
	public void testAdunare() {
		Monom x = new Monom(1, 7);
		Monom y = new Monom(123, 12);
		Monom rez = x.adunare(y);
		assertTrue(rez == null);
		
		x = new Monom(13, 2);
		y = new Monom(-13, 2);
		rez = x.adunare(y);
		assertTrue(rez.getCoeficient() == 0);
		
		x = new Monom(14, 2);
		y = new Monom(-13, 2);
		rez = x.adunare(y);
		assertTrue(rez.getCoeficient() == 1 && rez.getPutere() == 2);
	}
	
	@Test
	public void testScadere() {
		Monom x = new Monom(0, 7);
		Monom y = new Monom(123, 12);
		Monom rez = x.scadere(y);
		assertTrue(rez == null);
		
		x = new Monom(13, 2);
		y = new Monom(13, 2);
		rez = x.scadere(y);
		assertTrue(rez.getCoeficient() == 0);
		
		x = new Monom(14, 2);
		y = new Monom(-13, 2);
		rez = x.scadere(y);
		assertTrue(rez.getCoeficient() == 27 && rez.getPutere() == 2);
	}
	
	@Test
	public void testInmultire() {
		Monom x = new Monom(0, 0);
		Monom y = new Monom(12, 12);
		Monom rez = x.inmultire(y);
		assertTrue(rez == null);
		
		x = new Monom(2, 12);
		y = new Monom(3, 1);
		rez = x.inmultire(y);
		assertTrue(rez.getCoeficient() == 6 && rez.getPutere() == 13);
	}
	
	@Test
	public void testImpartire() {
		Monom x = new Monom(12, 12);
		Monom y = new Monom(0, 5);
		Monom rez = x.impartire(y);
		assertTrue(rez == null);
		
		x = new Monom(12, 12);
		y = new Monom(3, 1);
		rez = x.impartire(y);
		assertTrue(rez.getCoeficient() == 4 && rez.getPutere() == 11);
	}
	
	@Test
	public void testDerivare() {
		Monom x = new Monom(12, 0);
		Monom rez = x.derivare();
		assertTrue(rez == null);
		
		x = new Monom(3, 12);
		rez = x.derivare();
		assertTrue(rez.getCoeficient() == 36 && rez.getPutere() == 11);
	}
	
	@Test
	public void testIntegrare() {
		Monom x = new Monom(0, 0);
		Monom rez = x.integrare();
		assertTrue(rez == null);
		
		x = new Monom(26, 12);
		rez = x.integrare();
		assertTrue(rez.getCoeficient() == 2 && rez.getPutere() == 13);
	}

}
