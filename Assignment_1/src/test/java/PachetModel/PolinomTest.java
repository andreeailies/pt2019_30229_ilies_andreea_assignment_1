package PachetModel;

import static org.junit.Assert.*;

import org.junit.Test;

public class PolinomTest {

	@Test
	public void testAddMonom() {
		Polinom p1 = new Polinom();
		Monom x = new Monom(-2, 7);
		Monom y = new Monom(3, 3);
		p1.addMonom(x);
		assertTrue(p1.getMonomi().first().getCoeficient() == -2);
		assertTrue(p1.getMonomi().first().getPutere() == 7);
		
		p1.addMonom(y);
		assertTrue(p1.getMonomi().last().getCoeficient() == 3);
		assertTrue(p1.getMonomi().last().getPutere() == 3);
		
		x = new Monom(2, 7);
		p1.addMonom(x);
		assertTrue(p1.getMonomi().first().getCoeficient() == 3);
		assertTrue(p1.getMonomi().first().getPutere() == 3);
		
		
		y = new Monom(-13, 3);
		p1.addMonom(y);
		assertTrue(p1.getMonomi().first().getCoeficient() == -10);
		assertTrue(p1.getMonomi().first().getPutere() == 3);
		
	}
	
	@Test
	public void testAdunare() {
		Polinom p1 = new Polinom();
		Polinom p2 = new Polinom();
		Monom x = new Monom(-2, 7);
		Monom y = new Monom(2, 7);
		p1.addMonom(x);
		p2.addMonom(y);
		Polinom rez = p1.adunare(p2);
		assertTrue(rez.getMonomi().isEmpty());
		
		x = new Monom(123, 2);
		y = new Monom(-13, 2);
		p1 = new Polinom();
		p1.addMonom(x);
		p2 = new Polinom();
		p2.addMonom(y);
		rez = p1.adunare(p2);
		assertTrue(rez.getMonomi().first().getCoeficient() == 110);
		
	}
	
	@Test
	public void testScadere() {
		Polinom p1 = new Polinom();
		Polinom p2 = new Polinom();
		Monom x = new Monom(-2, 7);
		Monom y = new Monom(-2, 7);
		p1.addMonom(x);
		p2.addMonom(y);
		Polinom rez = p1.scadere(p2);
		assertTrue(rez.getMonomi().isEmpty());
		
		x = new Monom(123, 2);
		y = new Monom(-13, 2);
		p1 = new Polinom();
		p1.addMonom(x);
		p2 = new Polinom();
		p2.addMonom(y);;
		rez = p1.scadere(p2);
		assertTrue(rez.getMonomi().first().getCoeficient() == 136);
	}
	
	@Test
	public void testInmultire() {
		Polinom p1 = new Polinom();
		Polinom p2 = new Polinom();
		Monom x = new Monom(0, -4);
		Monom y = new Monom(2, 7);
		p1.addMonom(x);
		p2.addMonom(y);
		Polinom rez = p1.inmultire(p2);
		assertTrue(rez.getMonomi().isEmpty());
		
		x = new Monom(1, 2);
		y = new Monom(-13, 2);
		p1 = new Polinom();
		p1.addMonom(x);
		p2 = new Polinom();
		p2.addMonom(y);;
		rez = p1.inmultire(p2);
		assertTrue(rez.getMonomi().first().getCoeficient() == -13);
		assertTrue(rez.getMonomi().first().getPutere() == 4);
	}
	
	@Test
	public void testImpartire() {
		Polinom p1 = new Polinom();
		Polinom p2 = new Polinom();
		Monom x = new Monom(-2, 7);
		Monom y = new Monom( 0, 7);
		p1.addMonom(x);
		p2.addMonom(y);
		Polinom rez = p1.impartire(p2).get(0);
		assertTrue(rez.getMonomi().isEmpty());
		
		x = new Monom(123, 2);
		y = new Monom(-123, 1);
		p1 = new Polinom();
		p1.addMonom(x);
		p2 = new Polinom();
		p2.addMonom(y);;
		rez = p1.impartire(p2).get(0);
		Polinom rest = p1.impartire(p2).get(1);
		assertTrue(rez.getMonomi().first().getCoeficient() == -1);
		assertTrue(rez.getMonomi().first().getPutere() == 1);
		assertTrue(rest.getMonomi().isEmpty());
	}
	
	@Test
	public void testDerivare() {
		Polinom p1 = new Polinom();
		Monom x = new Monom(-2, 0);
		Monom y;
		p1.addMonom(x);
		Polinom rez = p1.derivare();
		assertTrue(rez.getMonomi().isEmpty());
		
		x = new Monom(123, 2);
		y = new Monom(-13, 5);
		p1 = new Polinom();
		p1.addMonom(x);
		p1.addMonom(y);
		rez = p1.derivare();
		assertTrue(rez.getMonomi().first().getCoeficient() == (-13 * 5));
		assertTrue(rez.getMonomi().last().getCoeficient() == 246);
		
		assertTrue(rez.getMonomi().first().getPutere() == 4);
		assertTrue(rez.getMonomi().last().getPutere() == 1);
	}
	
	@Test
	public void testIntegrare() {
		Polinom p1 = new Polinom();
		Monom x = new Monom(0, 0);
		Monom y;
		p1.addMonom(x);
		Polinom rez = p1.integrare();
		assertTrue(rez.getMonomi().isEmpty());
		
		x = new Monom(90, 2);
		y = new Monom(36, 5);
		p1 = new Polinom();
		p1.addMonom(x);
		p1.addMonom(y);
		rez = p1.integrare();
		assertTrue(rez.getMonomi().first().getCoeficient() == 6);
		assertTrue(rez.getMonomi().last().getCoeficient() == 30);
		
		assertTrue(rez.getMonomi().first().getPutere() == 6);
		assertTrue(rez.getMonomi().last().getPutere() == 3);
	}

}
