package PachetModel;

import org.junit.Test;

import junit.framework.TestCase;

public class ModelTest extends TestCase {
	private static Model m = new Model();
	
	@Test
	public void testTransformare() {
		String tester = "5x^ 2- 44* x ^3-x";
		Polinom p = m.transformare(tester);
		assertTrue(p.getMonomi().first().getCoeficient() == -44);
		assertTrue(p.getMonomi().last().getCoeficient() == -1);
		
		tester = "10";
		p = m.transformare(tester);
		assertTrue(p.getMonomi().first().getPutere() == 0 && p.getMonomi().first().getCoeficient() == 10);
	}
}
